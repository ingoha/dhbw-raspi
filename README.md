# dhbw-raspi

Hilfsskripte für die Installation von DB, MQTT und NodeJS auf einem RasPi (mit RaspiOS 11). Für die LV Softwareprojekt im Studiengang Elektrotechnik.

# Installation

An der Kommandozeile des Pi:

Git installieren
`sudo apt -y install git`

Danach Repository klonen
`git clone https://gitlab.com/ingoha/dhbw-raspi.git`

Im Anschluß das Installationsskript aufrufen
`cd dhbw-raspi && ./setup.sh`

# Überprüfen
## Datenbank
An der Kommandozeile des Pi:

`mysql -u root -p123456`

Erwartetes Ergebnis: `Welcome to the MariaDB monitor...`

Beenden mit `quit`

## MQTT
Hier bietet es sich an, zwei Konsolensitzungen zum Pi (z.B. per SSH) zu haben.

In der ersten Sitzung: `mosquitto_sub -v -t '#'`

In der zweiten: `mosquitto_pub -t 'TEST' -m 'Hallo'`

Erwartetes Ergebnis: Die Nachricht wird unter dem Befehl `mosquitto_sub` angezeigt. Er kann mittels der Tastenkombination `Strg+C` beendet werden.

## NodeJS
An der Kommandozeile des Pi:

`node -v`

Erwartetes Ergebnis: `v12.22.12`

An der Kommandozeile des Pi:

`npm -v`

Erwartetes Ergebnis: `7.5.2`
